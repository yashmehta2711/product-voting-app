import React from "react";
import Product from "./Product";
import Seed from "../seed";

/*
state is owned by the component.
'this.state' is private to the component, but we can update it.

Every React component is rendered as function of its "props" and "state". This rendering is known as Deterministic.

For all state modifications after the initial state, React provides components the method this.setState()
Always treat 'this.state' object as immutable, ie you cannot directly change it.
*/

class ProductList extends React.Component{
        state = {
            products: []
        };

    componentDidMount(){
        this.setState({products: Seed.products});
    }

    handleProductUpVote = (productId) => {
        // const products = this.state.products;
        // products.forEach((product) => {
        //     if(product.id === productId){
        //         product.votes += 1;
        //     }
        // });
        const newProducts = this.state.products.map((product) => {
            if(product.id === productId){
                return Object.assign({}, product, { votes: product.votes + 1 });
            }
            return product;
        });

        this.setState({products: newProducts});
    }

    render(){
        const products = this.state.products.sort((p1, p2) => (p2.votes - p1.votes));
        const productComponents = products.map((product) => (
            <Product
                key={'product-' + product.id}
                id={product.id}
                title={product.title}
                description={product.description}
                url={product.url}
                votes={product.votes}
                submitterAvatarUrl={product.submitterAvatarUrl}
                productImageUrl={product.productImageUrl}
                onUpVote={ this.handleProductUpVote }
            />
        ));
        return (
            <>
                <h1 className="ui dividing centered header">Product List</h1>
                <div className="ui unstackable items container">
                    {productComponents}
                </div>
            </>
        );
    }
}
/*
Note : {} inside JSX is know JavaScript Expressions
Product Component will get the props as follows : 
{
    "id" : 4,
    "title" : "Flatfile",
    "description" : "Some description",
}
React.createElement('Product', {id: , title}, null);

const array = ['one', 'two', 'three', ]
*/
export default ProductList;