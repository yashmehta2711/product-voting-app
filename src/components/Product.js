import React from "react";

/*
- All the props which is passed by the parent to the child can be accessed with the special object named as 'prop'.
- While the child can read the prop, it can't modify them. A child does not own its props.

- In React, the parent component owns the prop. (ProductList owns the props of Product)
- React favors the idea of One-Way-Data-Flow.
- Data changes come from the "top" of the app and are propogated "downwards" through its various components.

- Any time we define our own custom component methods, we have to manually bind 'this' to the component ourselves.
*/

class Product extends React.Component{

    handleUpVote = () => {
        // console.log(this);
        this.props.onUpVote(this.props.id);
    }

    render(){
        return(
            <div className="item">
                <div className="image">
                    <img src={this.props.productImageUrl}
                        alt={this.props.title} />
                </div>
                <div className="middle aligned content">
                    <div className="header">
                        <a onClick={this.handleUpVote}>
                            <i className="large caret up icon" />
                        </a>
                        {this.props.votes}
                    </div>
                    <div className="description">
                        <h3><a>{this.props.title}</a></h3>
                        <p>{this.props.description}</p>
                    </div>
                    <div className="extra">
                        <span>Submitted By:</span>
                        <img 
                            className="ui avatar image"
                            src={this.props.submitterAvatarUrl}
                            alt={this.props.title} 
                        />
                    </div>
                </div>
            </div>
        );
    }
}
export default Product